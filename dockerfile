#Creating a dockerfile by using the command
vi dockerfile

#Content to be written in the dockerfile
FROM centos
MAINTAINER bhuvaneswari gbhuvan199@gmail.com
RUN yum install java -y
RUN mkdir /opt/tomcat
WORKDIR /opt/tomcat
ADD https://dlcdn.apache.org/tomcat/tomcat-9/v9.0.64/bin/apache-tomcat-9.0.64.tar.gz .
RUN tar -xvzf apache-tomcat-9.o.64.tar.gz
RUN mv apache-tomcat-9.0.54/* /opt/tomcat
EXPOSE 8080
CMD ["/opt/tomcat/bin/catalina.sh", "run"]

#Create an image 
docker build -t mytomcat .

#To see the image 
docker images
